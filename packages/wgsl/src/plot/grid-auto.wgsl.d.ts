declare module "@use-gpu/wgsl/plot/grid-auto.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getGridAutoState: ParsedBundle;
  export default __module;
}
