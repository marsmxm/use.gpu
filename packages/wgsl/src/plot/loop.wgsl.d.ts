declare module "@use-gpu/wgsl/plot/loop.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const loopSurface: ParsedBundle;
  export default __module;
}
