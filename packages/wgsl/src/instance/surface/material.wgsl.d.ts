declare module "@use-gpu/wgsl/instance/surface/material.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getMaterialSurface: ParsedBundle;
  export default __module;
}
