declare module "@use-gpu/wgsl/instance/fragment/deferred-emissive.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getDeferredEmissiveFragment: ParsedBundle;
  export default __module;
}
