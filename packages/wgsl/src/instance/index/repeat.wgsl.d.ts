declare module "@use-gpu/wgsl/instance/index/repeat.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getInstanceRepeatIndex: ParsedBundle;
  export default __module;
}
