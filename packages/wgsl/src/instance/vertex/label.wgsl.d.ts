declare module "@use-gpu/wgsl/instance/vertex/label.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getLabelVertex: ParsedBundle;
  export default __module;
}
