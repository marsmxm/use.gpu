declare module "@use-gpu/wgsl/render/sample/equi-to-cube.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getEquiToCubeSample: ParsedBundle;
  export default __module;
}
