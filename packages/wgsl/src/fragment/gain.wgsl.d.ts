declare module "@use-gpu/wgsl/fragment/gain.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const gainColor: ParsedBundle;
  export default __module;
}
